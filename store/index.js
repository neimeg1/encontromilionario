export default {
	state(){
		return {
			chats:[{
				uuid: 1234,
				state: 1,
				messages:[]
			},{
				uuid: 1235,
				state: 1,
				messages:[]
			}]
		}
	},
	mutations: {
		ADD_CHAT(state, uuid){
			const chat = state.chats.find( c => c.uuid === uuid)

			if(!chat){
				state.chats.push({
					uuid,
					state: 1,
					messages:[]
				})
			}
		},
		TOGGLE_CHAT_WINDOW(state, data){
			const chat = state.chats.find( c => c.uuid === data.uuid )

			chat.state = data.open
		},
		CLOSE_CHAT_WINDOW(state, uuid){
			const index = state.chats.indexOf( c => c.uuid === uuid )

			state.chats.splice(index, 1)
		}
	},
	getters: {
		chats(state){
			return state.chats
		}
	}
}