const colors = require('tailwindcss/colors')

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      primary: {
        DEFAULT: '#48536A',
        '50': '#C2C8D6',
        '100': '#B3BBCB',
        '200': '#959FB7',
        '300': '#7684A2',
        '400': '#5D6A89',
        '500': '#48536A',
        '600': '#343B4C',
        '700': '#1F232E',
        '800': '#0A0C0F',
        '900': '#000000'
      },
      secondary: {
        DEFAULT: '#F6BA3A',
        '50': '#FFFFFF',
        '100': '#FFFEFD',
        '200': '#FDEDCC',
        '300': '#FADC9C',
        '400': '#F8CB6B',
        '500': '#F6BA3A',
        '600': '#F2A80B',
        '700': '#C18609',
        '800': '#906407',
        '900': '#604304'
      },
      ...colors,
    },
    extend: {
      
    },
  },
  variants: {
    extend: {},
  },
  corePlugins: {
    container: false
  },
  plugins: [
  function ({ addComponents }) {
      addComponents({
        '.container': {
          '@screen xl': {
            maxWidth: '1200px',
            width: '100%'
          },
        }
      })
    }
  ],
}
